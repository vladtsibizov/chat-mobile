import React from 'react';
import { Router, Stack, Scene } from 'react-native-router-flux';
import Login from './components/Login';
import Queue from './components/Queue';
import Dialog from './components/Dialog';
import Rate from './components/Rate';

const App = () => (
  <Router>
    <Stack key="root">
      <Scene key="rate-dialog" component={Rate} title="Оцените диалог" />
      <Scene key="dialog" component={Dialog} title="Диалог" />
      <Scene key="login" component={Login} title="Вход" />
      <Scene key="queue" component={Queue} title="Очередь" />
    </Stack>
  </Router>
);

export default App;
