import React, { useState } from 'react';
import PickerSelect from 'react-native-picker-select';
import { StyleSheet, Text, View, TextInput, Button } from 'react-native';

const Login = () => {
  const [theme, setTheme] = useState(undefined);
  const [subTheme, setSubTheme] = useState(undefined);
  return (
    <View style={styles.container}>
      <View>
        <Text>Введите имя:</Text>
        <TextInput style={styles.input} />
        <Text style={styles.pad}>Выберите тему обращения:</Text>
        <PickerSelect
          onValueChange={val => {
            setTheme(val);
          }}
          items={[
            { value: 'js', label: 'JavaScript' },
            { value: 'cpp', label: 'C++' },
            { value: 'c-sharp', label: 'C#' },
          ]}
        />
        <Text>Выберите подтему:</Text>
        <PickerSelect
          onValueChange={val => {
            setSubTheme(val);
          }}
          items={[
            { value: 'functions', label: 'functions' },
            { value: 'objects', label: 'objects' },
            { value: 'variables', label: 'variables' },
          ]}
        />
      </View>
      <Button
        onPress={() => {
          alert('clicked');
        }}
        title="Войти в чат"
      />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    padding: 20,
    flex: 1,
    justifyContent: 'space-between',
  },
  input: {
    borderWidth: 1,
    borderColor: 'grey',
    marginTop: 30,
  },
  pad: {
    marginTop: 30,
  },
});

export default Login;
