import React from 'react';
import { View, Text, StyleSheet, Image, Button } from 'react-native';

const Star = () => (
  <Image
    style={styles.star}
    source={{
      uri:
        'https://obychenie.herokuapp.com/images/%D0%BE%D1%86%D0%B5%D0%BD%D0%B8%D1%82%D1%8C/u4.png',
    }}
  />
);

const Rate = () => (
  <View style={styles.container}>
    <Text>Диалог завершен</Text>
    <Text>Оцените общение со специалистом</Text>
    <View style={styles.stars}>
      <Star />
      <Star />
      <Star />
      <Star />
      <Star />
    </View>
    <Button title="Оценить" />
  </View>
);

const styles = StyleSheet.create({
  container: {
    padding: 10,
    flex: 1,
    alignContent: 'space-between',
  },
  star: {
    width: 50,
    height: 50,
    margin: 8,
  },
  stars: {
    height: 30,
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
});

export default Rate;
