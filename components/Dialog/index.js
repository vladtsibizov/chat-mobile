import React, { useState, useRef } from 'react';
import {
  StyleSheet,
  View,
  Text,
  TextInput,
  TouchableOpacity,
  Image,
  ScrollView,
} from 'react-native';
import { Card } from 'native-base';
import dialog from '../../assets/dialog';

const imgReg = /(\b(https?|ftp|file):\/\/[-A-Z0-9+&@#\/%?=~_|!:,.;]*[-A-Z0-9+&@#\/%=~_|])/i;

const renderMessage = (msg, i) => {
  const ourMsg = msg.authorId === 1;
  const { text } = msg;
  return (
    <View
      key={i}
      style={[styles.msg, ourMsg ? styles.ourMsg : styles.theirMsg]}
    >
      <Card
        style={[
          styles.msgCard,
          ourMsg ? styles.ourMsgCard : styles.theirMsgCard,
        ]}
      >
        {imgReg.test(text) ? (
          <Image style={styles.imgInMsg} source={{ uri: text }} />
        ) : (
          <Text>{text}</Text>
        )}
      </Card>
    </View>
  );
};

const Dialog = () => {
  const ref = useRef(null);
  const handleChange = e => {
    setMessage(e.nativeEvent.text);
  };
  const handleSend = () => {
    setMessages(messages.concat({ authorId: 1, text: message }));
    setMessage('');
  };
  const [messages, setMessages] = useState(dialog.messages);
  const [message, setMessage] = useState('');
  const noMessages = !messages.length;
  return (
    <View style={styles.container}>
      <View>
        <View style={styles.header}>
          <View style={{ borderRightWidth: noMessages ? 0 : 1, padding: 10 }}>
            <Text>Вы общаетесь с оператором Имя</Text>
          </View>
          <View
            style={{
              padding: 10,
              display: noMessages ? 'none' : 'flex',
            }}
          >
            <Text>Завершить диалог</Text>
          </View>
        </View>
        <View>
          <Text style={styles.info}>Диалог начат в {dialog.created}</Text>
        </View>
      </View>
      <ScrollView
        ref={ref}
        onContentSizeChange={e => {
          ref.current.scrollToEnd({ animated: false });
        }}
        style={styles.messagesView}
      >
        {messages.map(renderMessage)}
      </ScrollView>
      <View style={styles.answerPanel}>
        <TextInput
          onChange={handleChange}
          value={message}
          style={styles.input}
          multiline={true}
        />
        <TouchableOpacity onPress={handleSend}>
          <Image
            style={styles.submitImg}
            source={{
              uri:
                'https://obychenie.herokuapp.com/images/%D0%B4%D0%B8%D0%B0%D0%BB%D0%BE%D0%B3_%D0%BD%D0%B0%D1%87%D0%B0%D0%BB%D0%BE/u5.png',
            }}
          />
        </TouchableOpacity>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    alignItems: 'center',
  },
  info: {
    textAlign: 'center',
    paddingTop: 10,
    paddingBottom: 5,
    borderBottomWidth: 0.5,
  },
  underline: {
    borderBottomWidth: 0.5,
    paddingBottom: 10,
  },
  answerPanel: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'flex-start',
  },
  input: {
    borderWidth: 0.5,
    height: 60,
    flex: 1,
    paddingHorizontal: 10,
  },
  submitImg: {
    width: 50,
    height: 60,
  },
  header: {
    flexDirection: 'row',
    borderBottomWidth: 0.5,
    justifyContent: 'space-between',
    alignItems: 'flex-start',
  },
  messagesView: {
    width: '100%',
    paddingHorizontal: 10,
  },
  msg: {
    flex: 1,
    flexDirection: 'row',
  },
  ourMsg: {
    justifyContent: 'flex-end',
  },
  theirMsg: {
    justifyContent: 'flex-start',
  },
  msgCard: {
    padding: 10,
    width: 'auto',
    maxWidth: '70%',
  },
  ourMsgCard: {
    backgroundColor: '#eee',
  },
  theirMsgCard: {
    backgroundColor: '#ddf',
  },
  imgInMsg: {
    width: '100%',
    aspectRatio: 1,
  },
});

export default Dialog;
