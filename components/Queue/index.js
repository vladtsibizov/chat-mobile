import React from 'react';
import { StyleSheet, View, Text, Image, Button } from 'react-native';

const Queue = () => {
  const text = `
Вы в очереди на 3 месте
Вам ответят приблизительно через
10 минут 10 cекунд  
`;
  return (
    <View style={styles.container}>
      <Text>{text}</Text>
      <View style={{ alignItems: 'center' }}>
        <Image
          style={styles.img}
          source={{
            uri:
              'https://obychenie.herokuapp.com/images/%D0%BE%D1%87%D0%B5%D1%80%D0%B5%D0%B4%D1%8C/u4.png',
          }}
        />
      </View>
      <Button style={styles.btn} title="Напомнить, когда придет очередь" />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    padding: 30,
    flex: 1,
    justifyContent: 'space-between',
  },
  btn: {
    marginTop: 30,
  },
  img: {
    width: 100,
    height: 100,
  },
});

export default Queue;
